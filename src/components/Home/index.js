import React from 'react';

import { withAuthorization } from '../Session';

const HomePage = () => (
  <div>
    <h1>Home Page</h1>
    <h3>Welcome to my app.</h3>
    <iframe
      width={560}
      height={315}
      src="https://www.youtube.com/embed/knW7-x7Y7RE"
      frameBorder={0} 
      allow="accelerometer;
       autoplay; 
       encrypted-media;
        gyroscope; 
        picture-in-picture" allowFullScreen />
  </div>
);

const condition = authUser => !!authUser;

export default withAuthorization(condition)(HomePage);