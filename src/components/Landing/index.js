/* eslint-disable jsx-a11y/iframe-has-title */
import React from 'react';

const Landing = () => (
    <div>
        <h1>Landing pages</h1>
        <br /><br />
        <iframe
            width ={480}
            height = {320} 
            src="https://www.youtube.com/embed/XGGWhOUYObc"
            frameBorder={1}
            allow="accelerometer;
            autoplay=1;
            encrypted-media;
            gyroscope; 
            " allowFullScreen={1}>
        </iframe>
    </div>
    )
    export default Landing